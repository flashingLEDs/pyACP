import sys, time
import serial
import os,datetime


def pollACP(filename,serialport):

	filename=filename
	serialport1=serialport

	# Calculate a timestamp
	now = time.time()
	localtime = time.localtime(now) 
	lastTime = now
	TimeStampVariable=("Timestamp:\t{0:02d}:{1:02d}:{2:02d}".format(localtime.tm_hour,localtime.tm_min,localtime.tm_sec))

	# Request a status update from the ACP
	# This assumes the factory default address (000) 
	serialport1.write("#000STA\r\n")
	response = serialport1.readline()

	data=response.rstrip('\n').split(",")
	if len(data)>0:
		statebits=data[1]
		faultbits=data[2]
		RPM=data[4]
		power=data[5]
		temperature=data[7]

		#Print the result to the console
		print("Timestamp\tStateBits\tFaultBits\tMotorRPM\tMotorPower\tTemperature")
		print("{0:02d}:{1:02d}:{2:02d}\t{3}\t\t{4}\t\t{5}\t\t{6}\t\t{7}\n".format(localtime.tm_hour,localtime.tm_min,localtime.tm_sec, statebits,faultbits,RPM,power,temperature))
	
		#Write the result to file
		f=open(filename,'a')
		f.write("{0:02d}:{1:02d}:{2:02d}\t{3}\t{4}\t{5}\t{6}\t{7}\n".format(localtime.tm_hour,localtime.tm_min,localtime.tm_sec, statebits,faultbits,RPM,power,temperature))
		f.close()

	else:
		print("{0:02d}:{1:02d}:{2:02d}\t !!!! No response from ACP !!!!\n".format(localtime.tm_hour,localtime.tm_min,localtime.tm_sec))



if __name__ == "__main__":

	try:
		portName=sys.argv[1]
		fileName=sys.argv[2]


	except:
		print("\nMissing argument\n\nExpected usage is monitorACP.py <portName> <saveFileName>")
		print("\tExample: monitorACP.py COM4 dataLog_001.txt")		
		sys.exit(1)

	serialPort=serial.Serial()
	serialPort.port=portName
	serialPort.baudrate=9600
	serialPort.bytesize=8
	serialPort.stopbits=1
	serialPort.parity=serial.PARITY_NONE
	serialPort.timeout=1

	try:
		serialPort.open()
	except:
		print("Couldn't access the (PC side) serial port. Do you have the port name correct? Is the RS485 adapter plugged in?")
		exit()


	f=open(filename,'w')
	f.write("Timestamp\tStateBits\tFaultBits\tMotorRPM\tMotorPower\tTemperature\n")
	f.close()
	
	while 1:
		pollACP(fileName,serialPort)
		time.sleep(3)